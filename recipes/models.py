from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125, null=True)
    author = models.CharField(max_length=100, null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    recipe = models.ForeignKey(Recipe, related_name="ingredients", on_delete=models.CASCADE)
    food = models.ForeignKey(FoodItem, on_delete=models.PROTECT)
    measure = models.ForeignKey(Measure, on_delete=models.PROTECT)
    amount = models.FloatField()

    def __str__(self):
        return self.food.name


class Step(models.Model):
    recipe = models.ForeignKey(Recipe, related_name="steps", on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField()
    directions = models.CharField(max_length=300, null=True)
    food_items = models.ManyToManyField("FoodItem", null=True, blank=True)

    def __str__(self):
        return f'{self.recipe.name} : Step {self.order}'
